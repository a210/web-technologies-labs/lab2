$(function () {
    $("#searchBar").autocomplete({
        source: (request, response) => {
            $.ajax({
                url: "https://secure.geobytes.com/AutoCompleteCity?key=7c756203dbb38590a66e01a5a3e1ad96&callback=?",
                beforeSend: function (request) {
                    request.setRequestHeader("Access-Control-Allow-Origin", "*");
                },
                type: "POST",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (data) {
                    response(data);
                }
            });
        }
    });
});



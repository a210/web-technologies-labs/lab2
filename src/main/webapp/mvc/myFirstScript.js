const pageTitle = document.getElementById("pageTitle");


// form values
const firstname = document.getElementById("firstname");
const lastname = document.getElementById("lastname");
const birthday = document.getElementById("birthday");
const isFemale = document.getElementById("sex2");
const isMale = document.getElementById("sex1");


// fieldset
function getFormSex() {
    return isFemale.checked ? 'F' : 'M';

}

function getFormMissingValues() {
    let missingValues = [];

    if (firstname.value.length < 1) {
        missingValues.push("firstname");
    }
    if (lastname.value.length < 1) {
        missingValues.push("lastname");
    }

    if (!birthday.value) {
        missingValues.push("birthday")
    }

    return missingValues
}


function submitForm(e) {
    e.preventDefault();
    const formStatus = getFormMissingValues();

    // form invalid
    if (formStatus.length > 0) {
        let message = "The form is missing the following values :\n";

        formStatus.map(v => message += ` - ${v} \n`);

        return alert(message)
    }

    //form valid
    message = `Form Values: \n` +
        `- firsname: ${firstname.value} \n` +
        `- lastname: ${lastname.value} \n` +
        `- birthday: ${birthday.value} \n` +
        `- sex: ${getFormSex()}`;
    return alert(message);
}


function toggleFM() {
    const fieldsetTags = document.getElementsByTagName("fieldset");
    const legendTags = document.getElementsByTagName("legend");
    const inputTags = document.getElementsByTagName("input");

    let styleFieldsetTag = "";
    let color = "";
    let titleText = "";

    // assigning the right styling according to sex
    if (isFemale.checked) {
        styleFieldsetTag = "3px solid blue";
        color = "blue";
        titleText = "Hello Madam";
    }
    if (isMale.checked) {
        styleFieldsetTag = "3px solid #ff2b68";
        color = "#ff2b68";
        titleText = "Hello Sir"
    }

    // setting the DOM elements with the right styling.
    for (let el of fieldsetTags) {
        el.style.border = styleFieldsetTag;
    }
    for (let el of legendTags) {
        el.style.color = color;
    }
    for (let el of inputTags) {
        el.style.borderColor = color;
    }
    pageTitle.innerText = titleText;
}



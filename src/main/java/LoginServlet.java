import com.google.gson.Gson;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private Gson gson = new Gson();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.addCrossOrigin(response);

        LoginDTO body = getBody(request);
        int isValid = body.getUsername().equals("abc") && body.getPassword().equals("123") ? 1 : 0;

        response.setContentType("text/plain");

        ServletOutputStream out = response.getOutputStream();
        out.println(isValid);
    }


    private LoginDTO getBody(ServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder();
        String s;
        while ((s = request.getReader().readLine()) != null) {
            sb.append(s);
        }
        LoginDTO dto = this.gson.fromJson(sb.toString(), LoginDTO.class);

        return dto;
    }

    private void addCrossOrigin(HttpServletResponse httpServletResponse) {
        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        httpServletResponse.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        httpServletResponse.addHeader("Access-Control-Max-Age", "1728000");
    }

}

# TP1 Java Servlet 


## Run for dev

- Install the maven dependencies via IntelliJ (registered in `pom.xml`)

- Add a run config for maven with command execution `tomcat7:run`. This will start a local Tomcat server (Maven plugin).

## Exercise 1
- run code and go to `http://localhost:9090/tp2/mvc/index.html`.

## Exercise 2
- run code and go to `http://localhost:9090/tp2/mvc/login.html`.

## Exercise 3 and 4
- run code and go to `http://localhost:9090/tp2/mvc/city.html`.



